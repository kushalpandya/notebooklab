import Vue from 'vue';
import json from '../../../fixtures/file.json';
import PromptComponent from '../prompt';

const cell = json.cells[0];
const Component = Vue.extend(PromptComponent);

describe('Prompt component', () => {
  let vm;

  test('input', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          type: 'input',
          count: 1,
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('renders in label', () => {
      expect(vm.$el.textContent.trim()).toContain('In');
    });

    test('renders count', () => {
      expect(vm.$el.textContent.trim()).toContain('1');
    });
  });

  test('output', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          type: 'output',
          count: 1,
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('renders in label', () => {
      expect(vm.$el.textContent.trim()).toContain('Out');
    });

    test('renders count', () => {
      expect(vm.$el.textContent.trim()).toContain('1');
    });
  });
});
