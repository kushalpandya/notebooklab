import Vue from 'vue';
import json from '../fixtures/file.json';
import NotebookLab from '../index';

Vue.use(NotebookLab);

describe('install function', () => {
  test('creates global component', () => {
    expect(
      Vue.options.components['notebook-lab']
    ).not.toBeNull();
  });
});
